/*
 * If not stated otherwise in this file or this component's Licenses.txt file the
 * following copyright and licenses apply:
 *
 * Copyright 2015 Zoltan Kuscsik <kuscsik@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include "virtual_input_device.h"
#include <linux/input.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#ifdef __ANDROID__
#include <cutils/properties.h>
#endif


static void getScreenResolution(unsigned long *widht, unsigned long *height) {
#ifdef __ANDROID__
    char *cp, property[PROPERTY_VALUE_MAX];
    unsigned long x =0 , y = 0;
    property_get("persist.sys.display.resolution", property, "800x600");
    x = strtoul(property, &cp, 10);
    if( *cp == 'x')
        y = strtoul(cp+1, &cp , 10);

    if (x!=0 || y!=0) {
        *widht = x;
        *height = y;
    }
#else
    #error("Only Android supported at this moment")
#endif
    
}

static int  init(char* name, int32_t busType) {
    int mFd;
    struct uinput_user_dev mUIDev;
    int i, error;
    unsigned long screen_width = 0 , screen_height = 0;

    getScreenResolution(&screen_width, &screen_height);
    mFd = open(UINPUT_DEVICE_FILE, O_WRONLY | O_NONBLOCK);

    if(mFd < 0) {
        printf("Failed to open mUIDevut\n");
        goto err;
    }

    // Setup the mUIDevut device
    ioctl(mFd, UI_SET_EVBIT, EV_KEY);
    ioctl(mFd, UI_SET_EVBIT, EV_REL);
    ioctl(mFd, UI_SET_EVBIT, EV_SYN);
    
	// Touch
	ioctl (mFd, UI_SET_EVBIT,  EV_ABS);
	ioctl (mFd, UI_SET_ABSBIT, ABS_MT_SLOT);
   	ioctl (mFd, UI_SET_ABSBIT, ABS_MT_SLOT);
	ioctl (mFd, UI_SET_ABSBIT, ABS_MT_TOUCH_MAJOR);
	ioctl (mFd, UI_SET_ABSBIT, ABS_MT_POSITION_X);
	ioctl (mFd, UI_SET_ABSBIT, ABS_MT_POSITION_Y);
	ioctl (mFd, UI_SET_ABSBIT, ABS_MT_TRACKING_ID);
	ioctl (mFd, UI_SET_ABSBIT, ABS_MT_PRESSURE);    
	ioctl (mFd, UI_SET_PROPBIT, INPUT_PROP_DIRECT);

	memset(&mUIDev, 0, sizeof(mUIDev));
    strncpy(mUIDev.name, name, UINPUT_MAX_NAME_SIZE);

    mUIDev.id.vendor  = 0x1;
    mUIDev.id.product = 0x1;
    mUIDev.id.bustype = busType;
    mUIDev.id.version = 4;
	mUIDev.id.bustype = BUS_USB;
	mUIDev.absmin[ABS_MT_SLOT] = 0;
	mUIDev.absmax[ABS_MT_SLOT] = 9;
	mUIDev.absmin[ABS_MT_TOUCH_MAJOR] = 0;
	mUIDev.absmax[ABS_MT_TOUCH_MAJOR] = 15;
	mUIDev.absmin[ABS_MT_POSITION_X] = 0;
	mUIDev.absmax[ABS_MT_POSITION_X] = (int) screen_width; 
	mUIDev.absmin[ABS_MT_POSITION_Y] = 0;
	mUIDev.absmax[ABS_MT_POSITION_Y] = (int) screen_height; 
	mUIDev.absmin[ABS_MT_TRACKING_ID] = 0;
	mUIDev.absmax[ABS_MT_TRACKING_ID] = 65535;
	mUIDev.absmin[ABS_MT_PRESSURE] = 0;
	mUIDev.absmax[ABS_MT_PRESSURE] = 255;


    write(mFd, &mUIDev, sizeof(mUIDev));
    printf("Creating touch device with size [%lux%lu]\n", screen_width, screen_height);

    if (ioctl(mFd, UI_DEV_CREATE) < 0)
    {
        goto err;
    }

    return mFd;

err:
    printf("Exiting with error\n");
    if(mFd >= 0)
        close(mFd);

    return -1;
}

static int sendEvent(int mFd, uint16_t type, uint16_t code, int32_t value) {
    struct input_event event;
    if( mFd<0 )
        return VIRT_DEV_FAILURE;

    memset(&event, 0, sizeof(event));
    gettimeofday(&event.time, NULL);
    event.type = type;  
    event.code = code;
    event.value = value;
    write(mFd, &event, sizeof(event));

    return  VIRT_DEV_SUCCESS;
}

static int closeFd(int mFd) {

    if(ioctl(mFd, UI_DEV_DESTROY) < 0)
        return VIRT_DEV_FAILURE;
    else
        return VIRT_DEV_SUCCESS; 
}

virt_input_dev_t virtualnputDev = {
    .initInputVirtDev = init,
    .closeInpuVirtDev = closeFd,
    .sendEvent = sendEvent,
};

virt_input_dev_t* getVirtualInputDevice() {
    return &virtualnputDev;
}
