/*
 * If not stated otherwise in this file or this component's Licenses.txt file the
 * following copyright and licenses apply:
 *
 * Copyright 2015 Zoltan Kuscsik <kuscsik@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#define ATRACE_TAG ATRACE_TAG_INPUT
#include <cutils/trace.h>

#include <stdio.h>
#include <math.h>
#include "virtual_input_device.h"
#include <unistd.h>

#define SCREEN_SIZE_X 3840
#define SCREEN_SIZE_Y 2160

virt_input_dev_t* vd;

void touch_release(int fd) {
        ATRACE_BEGIN("touch_release");
        vd->sendEvent(fd, EV_ABS, ABS_MT_SLOT, 0);
    	vd->sendEvent(fd, EV_ABS, ABS_MT_TRACKING_ID, -1);
    	vd->sendEvent(fd, EV_SYN, SYN_REPORT, 0x0);
    //   	vd->sendEvent(fd, EV_SYN, SYN_DROPPED, 0x0);

        ATRACE_END();
}

void touch(int fd, int id, int x, int y) {
        ATRACE_BEGIN("touch");
    	vd->sendEvent(fd, EV_ABS, ABS_MT_TRACKING_ID, id);
    	vd->sendEvent(fd, EV_ABS, ABS_MT_POSITION_X, x);
    	vd->sendEvent(fd, EV_ABS, ABS_MT_POSITION_Y, y);
    	vd->sendEvent(fd, EV_ABS, ABS_MT_PRESSURE, 0x33);
    	vd->sendEvent(fd, EV_SYN, SYN_REPORT, 0x0);
        ATRACE_END();
}


int draw_circle(int fd, int sx, int sy, int r, int pause) {
    int i;

 for(i = 1 ; i <= 500; i++) {
         
	touch(fd, 20,
		sx/2 + (r - i / 5) * sin( 2 * M_PI * i /100.0), 
		sy/2 + (r - i / 5) * cos( 2 * M_PI * i / 100.0)
		);

	usleep(16000);

	if( (pause > 0) && (i % pause == 0) ) {
        touch_release(fd);
		usleep(160000);
    }
 }
 touch_release(fd);
    return 0;
}

int main() {
    vd = getVirtualInputDevice();
    int fd = vd->initInputVirtDev("TOUCH_DEV", BUS_USB);

    printf("Press Any Key to draw a circle using multiple touch sequence\n");
    getchar();    
    draw_circle(fd, SCREEN_SIZE_X, SCREEN_SIZE_Y, 500, 10);
    printf("Press Any Key to draw a circle using one touch sequence\n");
    getchar();    

    draw_circle(fd, SCREEN_SIZE_X, SCREEN_SIZE_Y, 300, 0);

    getchar();    
     return 0;
};

