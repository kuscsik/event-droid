#ifndef VIRTDEV_VIRTUALKEYBOARD_H
#define VIRTDEV_VIRTUALKEYBOARD_H

extern "C" {
#include <cutils/log.h>
#include <utils/Errors.h>
#include <linux/uinput.h>
#include <fcntl.h>
}

#include "VirtualInputDevice.h"

using namespace android;

namespace virtdev {

#define UINPUT_DEVICE_FILE "/dev/uinput"


class VirtualKeyboard : public VirtualInputDevice{
public:
    VirtualKeyboard(char* name);
    ~VirtualKeyboard();
    void pressKey(int key);
    void setKeyReleaseTime();

private:
    int mKeyReleaseTyme;
};

}

#endif
