/**
 *  Author: Zoltan Kuscsik <kuscsik@gmail.com>
 */

#ifndef VIRTDEV_VIRTUALINPUTDEVICE_H
#define VIRTDEV_VIRTUALINPUTDEVICE_H

#include <linux/uinput.h>
#include <fcntl.h>

#define VIRT_DEV_SUCCESS 1
#define VIRT_DEV_FAILURE 0
#define UINPUT_DEVICE_FILE "/dev/uinput"

typedef struct virt_input_dev_t{
	int (*initInputVirtDev)(char* name, int32_t busType);
	int (*closeInpuVirtDev)();
	int (*sendEvent)(int mFd, uint16_t type, uint16_t code, int32_t value);
} virt_input_dev_t;

virt_input_dev_t* getVirtualInputDevice();

#endif
