LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS:=optional
LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/include

LOCAL_SRC_FILES:=\
        src/virtual_input_device.c


LOCAL_MODULE := libeventdroid
LOCAL_STATIC_LIBRARIES := libcutils libc 
include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS:=optional
LOCAL_C_INCLUDES := \
    $(LOCAL_PATH)/include

LOCAL_SRC_FILES := \
    src/virtual_input_device.c \
    test/test_virtualmultitouch.c

LOCAL_MODULE := test_virtualmultitouch
LOCAL_SHARED_LIBRARIES := libcutils
include $(BUILD_EXECUTABLE)
